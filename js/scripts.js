var botao = document.getElementById('button1');
var myLatLng = 0;
var myLatLngArray = [];
var timestamp = [];
var interval = 0;

var count = 0;

var marker; // Declare a variável marker fora da função

function initMap() {
    // Crie um objeto LatLng com as coordenadas do local onde você deseja adicionar o marcador
    // var myLatLng = { lat: -34.397, lng: 150.644 };

    var myLatLng = {lat: -1.4758618, lng: -48.457288};

    // var myLatLng = {lat: -1.4753187, lng: -48.452834};
  
    // Crie um objeto de opções para o mapa
    var mapOptions = {
      zoom: 17,
      center: myLatLng // Defina o centro do mapa para as coordenadas desejadas
    };
  
    // Crie um novo mapa e o anexe a um elemento HTML com o ID "map"
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
  
    // Crie um marcador e configure suas opções
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map, // Associe o marcador ao mapa que você criou
      title: 'Meu Marcador' // Título exibido quando você passa o mouse sobre o marcador
    });
  }

  botao.addEventListener('click', function() {

    if(count != 0) {
        stopBlinking();
    }
    // Crie um objeto LatLng com as coordenadas do local onde você deseja adicionar o marcador
    var myLatLng = vet();
    myLatLngArray.push(myLatLng);

    timestamp.push(Date.now());

    // console.log(myLatLngArray);
  
    // Crie um objeto de opções para o mapa
    var mapOptions = {
      zoom: 17,
      center: myLatLng // Defina o centro do mapa para as coordenadas desejadas
    };
  
    // Crie um novo mapa e o anexe a um elemento HTML com o ID "map"
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var icon = {
        url: "http://localhost/instant-help/img/alert.png", // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };
  
    // Crie um marcador e configure suas opções
    marker = new google.maps.Marker({
      position: myLatLng,
      map: map, // Associe o marcador ao mapa que você criou
      title: 'Meu Marcador', // Título exibido quando você passa o mouse sobre o marcador
      icon: icon
    });
  
    // Inicie a função para fazer o marcador piscar
    startBlinking();

    count++;

    // Selecione o elemento da lista
    var listaOcorrencias = document.getElementById('ocorrencias-list');

    // Limpe o conteúdo atual da lista
    listaOcorrencias.innerHTML = '';

    data = Date.now();

    // Adicione dinamicamente os itens da lista com base nos dados de ocorrências
    myLatLngArray.forEach(function (ocorrencia, index) {
      var listItem = document.createElement('a');
      listItem.href = '#';
      listItem.classList.add('list-group-item', 'list-group-item-action');
      listItem.textContent = "Posição: (" + ocorrencia["lat"] + "," + ocorrencia["lng"] + ")" +
                              " --- Registro : " + new Date(timestamp[index]).toLocaleDateString("pt-br") +
                              " " +new Date(timestamp[index]).toLocaleTimeString("pt-br");
    
      // Adicione o item à lista
      listaOcorrencias.appendChild(listItem);
    });
  });

  function startBlinking() {
    interval = setInterval(function () {
      marker.setVisible(!marker.getVisible());
    }, 500); // Alterne a visibilidade a cada segundo (1000 ms)
  }
  
  // Chame esta função para parar o piscar
  function stopBlinking() {
    clearInterval(interval);
    marker.setVisible(true); // Garanta que o marcador esteja visível no final
  }

function vet() {
    var vet = [{lng: -48.458029,lat: -1.472561},
        {lng: -48.456291,lat: -1.472840},
        {lng: -48.458094,lat: -1.473859},
        {lng: -48.456323,lat: -1.474224},
        {lng: -48.457793,lat: -1.474878},
        {lng: -48.457611,lat: -1.476637},
        {lng: -48.456034,lat: -1.476723},
        {lng: -48.456817,lat: -1.475918},
        {lng: -48.456066,lat: -1.475414},
        {lng: -48.456452,lat: -1.474878},
        {lng: -48.456742,lat: -1.473537},
        {lng: -48.455036,lat: -1.476326},
        {lng: -48.455443,lat: -1.474254},
        {lng: -48.455539,lat: -1.472838},
        {lng: -48.454434,lat: -1.473535},
        {lng: -48.454466,lat: -1.474500},
        {lng: -48.453898,lat: -1.475530},
        {lng: -48.453479,lat: -1.475777},
        {lng: -48.452986,lat: -1.475745},
        {lng: -48.452052,lat: -1.475434},
        {lng: -48.451205,lat: -1.475069},
        {lng: -48.450218,lat: -1.474543},
        {lng: -48.449745,lat: -1.474199},
        {lng: -48.449198,lat: -1.473941},
        {lng: -48.448747,lat: -1.473630},
        {lng: -48.449305,lat: -1.473062},
        {lng: -48.449369,lat: -1.472354},
        {lng: -48.449520,lat: -1.471067},
        {lng: -48.450303,lat: -1.471474},
        {lng: -48.449766,lat: -1.472933},
        {lng: -48.450603,lat: -1.472247},
        {lng: -48.450710,lat: -1.472997},
        {lng: -48.449970,lat: -1.473738},
        {lng: -48.451086,lat: -1.473920},
        {lng: -48.451472,lat: -1.473298},
        {lng: -48.452524,lat: -1.473705},
        {lng: -48.453360,lat: -1.473941},
        {lng: -48.454015,lat: -1.474038},
        {lng: -48.454058,lat: -1.473394},
        {lng: -48.453425,lat: -1.475126},
        {lng: -48.452760,lat: -1.475147},
        {lng: -48.452481,lat: -1.474933},
        {lng: -48.452931,lat: -1.474718},
        {lng: -48.452931,lat: -1.474397},
        {lng: -48.452105,lat: -1.474418},
        {lng: -48.452148,lat: -1.474911},
        {lng: -48.451783,lat: -1.474847},
        {lng: -48.451301,lat: -1.474729},
        {lng: -48.450389,lat: -1.474268},
        {lng: -48.451719,lat: -1.474386},
        {lng: -48.450039,lat: -1.474033},
        {lng: -48.449481,lat: -1.473775},
        {lng: -48.449288,lat: -1.473260}]

    return vet[parseInt(Math.random() * vet.length)];
}